package cpuscheduler;
/**
 * Enum containing every state of process
 */
public enum State {
    NEW,
    READY,
    RUNNING,
    WAITING,
    TERMINATED
}
